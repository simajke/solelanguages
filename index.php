<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('School Of Linguistic Experience');
?>
<?
// $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
// 	"AREA_FILE_SHOW" => "page",
// 	"AREA_FILE_SUFFIX" => "example",
// 	"EDIT_TEMPLATE" => ""
// )
// );
?>
<!-- Banner section -->
<section class="sole-banner sole-banner__position">
	<div class="transparent-layer absolute-center h-100 w-100"></div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="sole-banner__content-wrapper sole-banner__content-wrapper_position">
						<div class="sole-banner__upper-text sole-banner__upper-text_position">
							<h4>WELCOME TO SOLE!</h4>
						</div>
						<div class="sole-banner__main-text_wrapper sole-banner__main-text_wrapper-positon">
							<h1 class="sole-banner__main-text nunito-black">Come learn languages in Sicily and explore its beauty.</h1>
						</div>
						<div class="sole-banner__button-wrapper"><a href="" class="sole-banner__button">LEARN MORE</a></div>
					</div>
				</div>
			</div>
		</div>
</section>
<!-- Moto section -->
<section>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="moto__main-wrapper moto__main-wrapper_position sole-banner__content-wrapper">
					<div class="moto__content-wrapper">
						<h2 class="moto__content">We believe that language learning and acquisition must go beyond the classroom to truly take hold.</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- What's happening section -->
<section class="whats-section whats-section__position">
	<div class="container">
		<div class="row">
			<div class="col-8">
				<div class="whats__left-wrapper sole-banner__content-wrapper">
					<div class="whats__headline_outer-wrapper whats__headline_outer-wrapper-position">
						<div class="whats__headline-wrapper">
							<h2 class="nunito-black">WHAT'S HAPPENING</h2>
						</div>
						<div class="moto__divider-wrapper_outter">
							<div class="moto__divider-wrapper_inner">
								<div class="moto__divider d-inline-block">
									
								</div>
							</div>
						</div>
					</div>
					<div class="whats__second-headline_wrapper whats__second-headline_wrapper-position">
						<h4 class="nunito-black">Autumn Schedule</h4>
					</div>
					<div class="whats__additional-info_wrapper">
						<span class="h5 whats__additional-info">Contact us for further information at<br />solelanguages@gmail.com</span>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="whats__right-wrapper">
					<div class="whats__img-wrapper">
						<img class="img-fluid" src="<?php echo SITE_TEMPLATE_PATH . "/assets/img/pizza.jpeg" ?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Why sole -->
<section class="why-sole__section">
	<div class="container">
		<div class="row justify-content-around justify-content-xl-start">
			<div class="col-12">
				<div class="why-sole__text-wrapper why-sole__text-wrapper_position sole-banner__content-wrapper">
					<div class="why-sole__top-text_wrapper whats__second-headline_wrapper-position">
						<span class="why-sole__top-text nunito-regular">WE'RE HAPPY TO SEE YOU</span>
					</div>
					<div class="why-sole__headline-wrapper">
						<h2 class="why-sole__headline nunito-black">WHY SOLE</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col-md-4 col-xl-2and4">
				<div class="card">
				  <img class="img-fluid why-sole card-img-top sole-banner__upper-text_position" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/why1.png' ?>">
				  <div class="card-body">
				    <h5 class="card-title nunito-black">Confident communication</h5>
				    <div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider moto__divider_for-small-block d-inline-block">
								
							</div>
						</div>
					</div>
				  </div>
				</div>
			</div>
			<div class="col-md-4 col-xl-2and4">
				<div class="card">
				  <img class="img-fluid why-sole card-img-top sole-banner__upper-text_position" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/why2.png' ?>">
				  <div class="card-body">
				    <h5 class="card-title nunito-black">Teachers who love language</h5>
				    <div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider moto__divider_for-small-block d-inline-block">
								
							</div>
						</div>
					</div>
				  </div>
				</div>
			</div>
			<div class="col-md-4 col-xl-2and4">
				<div class="card">
				  <img class="img-fluid why-sole card-img-top sole-banner__upper-text_position" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/why3.png' ?>">
				  <div class="card-body">
				    <h5 class="card-title nunito-black">Cultural activities</h5>
				    <div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider moto__divider_for-small-block d-inline-block">
								
							</div>
						</div>
					</div>
				  </div>
				</div>
			</div>
			<div class="col-md-4 col-xl-2and4">
				<div class="card">
				  <img class="img-fluid why-sole card-img-top sole-banner__upper-text_position" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/why4.png' ?>">
				  <div class="card-body">
				    <h5 class="card-title nunito-black">Outdoor activities</h5>
				    <div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider moto__divider_for-small-block d-inline-block">
								
							</div>
						</div>
					</div>
				  </div>
				</div>
			</div>
			<div class="col-md-4 col-xl-2and4">
				<div class="card">
				  <img class="img-fluid why-sole card-img-top sole-banner__upper-text_position" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/why5.png' ?>">
				  <div class="card-body">
				    <h5 class="card-title nunito-black">Sustainable tourism</h5>
				    <div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider moto__divider_for-small-block d-inline-block">
								
							</div>
						</div>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Courses section -->
<section class="sole-courses__section">
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-12">
				<div class="why-sole__text-wrapper why-sole__text-wrapper_position sole-courses__text-wrapper sole-courses__text-wrapper-position sole-banner__content-wrapper">
					<div class="why-sole__top-text_wrapper whats__second-headline_wrapper-position">
						<span class="why-sole__top-text nunito-regular">ITALIAN & ENGLISH COURSES</span>
					</div>
					<div class="why-sole__headline-wrapper">
						<h2 class="why-sole__headline nunito-black">COURSES</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col-3">
				<div class="card">
				  <div class="card-body">
				  	<div class="sole-courses__headline-wrapper sole-banner__upper-text_position">
				    	<h4 class="nunito-black card-title">Italian Conversation and Experience</h4>
				    </div>
				    <a class="sole-courses__card-button d-inline-block" href="">
					    <span class="d-flex">
					    	<span>LEARN MORE</span>
					    	<span class="ml-1">
					    		<i class="fa fa-long-arrow-right"></i>
					    	</span>
					    </span>
				    </a>
				  </div>
				</div>
			</div>
			<div class="col-3">
				<div class="card">
				  <div class="card-body">
				  	<div class="sole-courses__headline-wrapper sole-banner__upper-text_position">
				    	<h4 class="nunito-black card-title">Aviation Italian</h4>
				    </div>
				    <a class="sole-courses__card-button d-inline-block" href="">
					    <span class="d-flex">
					    	<span>LEARN MORE</span>
					    	<span class="ml-1">
					    		<i class="fa fa-long-arrow-right"></i>
					    	</span>
					    </span>
				    </a>
				  </div>
				</div>
			</div>
			<div class="col-3">
				<div class="card">
				  <div class="card-body">
				  	<div class="sole-courses__headline-wrapper sole-banner__upper-text_position">
				    	<h4 class="nunito-black card-title">English Conversation</h4>
				    </div>
				    <a class="sole-courses__card-button d-inline-block" href="">
					    <span class="d-flex">
					    	<span>LEARN MORE</span>
					    	<span class="ml-1">
					    		<i class="fa fa-long-arrow-right"></i>
					    	</span>
					    </span>
				    </a>
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Teachers section -->
<section class="sole-teachers__container">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="why-sole__text-wrapper why-sole__text-wrapper_position sole-courses__text-wrapper sole-courses__text-wrapper-position sole-banner__content-wrapper">
					<div class="why-sole__top-text_wrapper whats__second-headline_wrapper-position">
						<span class="why-sole__top-text nunito-regular">MEET OUR TEACHERS</span>
					</div>
					<div class="why-sole__headline-wrapper">
						<h2 class="why-sole__headline nunito-black">TEACHERS</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col-3">
				<div class="sole-teachers__teacher-img-wrapper">
					<img class="img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/mal.jpg' ?>" alt="" class="sole-teachers__teacher-img">
				</div>
			</div>
			<div class="col-9 d-flex align-items-center">
				<div class="sole-teachers__inner-wrapper">
					<div class="sole-teachers__headline-wrapper sole-banner__upper-text_position">
						<h4 class="nunito-black card-title m-0">Mallory Echols</h4>
					</div>
					<div class="sole-teachers__text-wrapper sole-banner__upper-text_position">
						<span class="h5">English teacher, the founder and director of SOLE.</span>
					</div>
					<div class="sole-teachers__link-wrapper">
						<div class="sole-teachers__link">
							
						</div>
					</div>
					<a class="sole-courses__card-button d-inline-block" href="">
					    <span class="d-flex">
					    	<span>READ MORE</span>
					    	<span class="ml-1">
					    		<i class="fa fa-long-arrow-right"></i>
					    	</span>
					    </span>
				    </a>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="offset-3 col-9">
				<div class="sole-teachers__bottom-wrapper">
					<div class="sole-teachers__headline-wrapper sole-banner__upper-text_position">
						<h4 class="nunito-black card-title m-0">We're searching for an Italian Teacher</h4>
					</div>
					<div class="sole-teachers__text-wrapper">
						<span class="h5">SOLE is currently searching for the perfect Italian teacher for our experiential language learning philosophy. Are you a native Italian speaker who has a passion for teaching in and outside of the classroom? Contact us at solelanguages@gmail.com</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Bottom picture -->
<section>
	<div class="container-fluid position-relative">
		<div class="transparent-layer absolute-center h-100 w-100"></div>
		<div class="sole__bottom-banner">
			
		</div>
	</div>
</section>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>