<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('School Of Linguistic Experience');
?>
<?
$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "page",
	"AREA_FILE_SUFFIX" => "example",
	"EDIT_TEMPLATE" => ""
)
);
?>
<!-- Banner section -->
<section class="category-courses__banner sole-banner position-relative">
	<div class="transparent-layer absolute-center h-100 w-100"></div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="category-courses__banner-text-wrapper sole-banner__content-wrapper sole-banner__content-wrapper_position">
					<div class="sole-banner__upper-text sole-banner__upper-text_position">
						<h4>ITALIAN & ENGLISH COURSES</h4>
					</div>
					<div class="sole-banner__main-text_wrapper sole-banner__main-text_wrapper-positon">
						<h1 class="category-courses__main-text sole-banner__main-text nunito-black">Courses</h1>
						<div class="moto__divider-wrapper_outter">
							<div class="moto__divider-wrapper_inner">
								<div class="moto__divider d-inline-block">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- list of courses -->
<section class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<div class="moto__main-wrapper moto__main-wrapper_position sole-banner__content-wrapper pb-3 pt-0">
							<h2>Italian Conversation and Experience</h2>
							<div class="moto__divider-wrapper_outter">
								<div class="moto__divider-wrapper_inner">
									<div class="moto__divider d-inline-block">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w-100"></div>
					<div class="col-6">
						<div class="list-courses__img-wrapper">
							<img class="list-courses__img img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/placeholder.png' ?>" alt="">
						</div>
					</div>
					<div class="col-6">
						<div class="mb-3">
							<p class="h5">
								Italian course for all levels combining the classroom experience with the community experience. Instruction will be a combination of traditional classroom principles and community interaction.
							</p>
							<p class="h5">
								To apply for the course or request more information, contact us.
							</p>
						</div>
						<div class="sole-banner__button-wrapper">
							<a href="" class="sole-banner__button sole-banner__button_courses">
								<span>LEARN MORE</span>
								<span class="ml-1">
						    		<i class="fa fa-long-arrow-right"></i>
						    	</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<div class="moto__main-wrapper moto__main-wrapper_position sole-banner__content-wrapper pb-3">
							<h2>Aviation Italian Course</h2>
							<div class="moto__divider-wrapper_outter">
								<div class="moto__divider-wrapper_inner">
									<div class="moto__divider d-inline-block">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w-100"></div>
					<div class="col-6">
						<div class="list-courses__img-wrapper">
							<img class="list-courses__img img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/placeholder.png' ?>" alt="">
						</div>
					</div>
					<div class="col-6">
						<div class="mb-3">
							<p class="h5">
								Targeted for members of the aviation industry seeking to take their airline’s or company’s Italian language test. This course will focus on aviation specific terminology as well as developing onboard conversation skills with both Italian-speaking ground crew and passengers.
							</p>
							<p class="h5">
								To apply for the course or request more information, contact us.
							</p>
						</div>
						<div class="sole-banner__button-wrapper">
							<a href="" class="sole-banner__button sole-banner__button_courses">
								<span>LEARN MORE</span>
								<span class="ml-1">
						    		<i class="fa fa-long-arrow-right"></i>
						    	</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-12">
						<div class="moto__main-wrapper moto__main-wrapper_position sole-banner__content-wrapper pb-3">
							<h2>English Conversation</h2>
							<div class="moto__divider-wrapper_outter">
								<div class="moto__divider-wrapper_inner">
									<div class="moto__divider d-inline-block">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="w-100"></div>
					<div class="col-6">
						<div class="list-courses__img-wrapper">
							<img class="list-courses__img img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/placeholder.png' ?>" alt="">
						</div>
					</div>
					<div class="col-6">
						<div class="mb-3">
							<p class="h5">
								Individual or group English course organized on demand. Focus will be prioritized to conversation and real-world scenarios.
							</p>
							<p class="h5">
								To apply for the course or request more information, contact us.
							</p>
						</div>
						<div class="sole-banner__button-wrapper">
							<a href="" class="sole-banner__button sole-banner__button_courses">
								<span>LEARN MORE</span>
								<span class="ml-1">
						    		<i class="fa fa-long-arrow-right"></i>
						    	</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>