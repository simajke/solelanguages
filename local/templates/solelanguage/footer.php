<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?php
	use Bitrix\Main\Page\Asset;

	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/popper.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/assets/js/bootstrap.min.js");
?>
		<footer class="pb-5">
			<div class="container">
				<nav class="navbar px-0">
				    <ul class="navbar-nav d-flex flex-row w-100">
				      <li class="nav-item">
				        <a class="nav-link" href="#">Home</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">Courses</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">About Us</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="#">Contact</a>
				      </li>
				    </ul>
				</nav>
				<div class="row">
					<div class="col-12 mb-4">
						<div class="moto__divider-wrapper_outter">
							<div class="moto__divider-wrapper_inner">
								<div class="moto__divider moto__divider_footer d-inline-block w-100"></div>
							</div>
						</div> 
					</div>
					<div class="w-100"></div>
					<div class="col-4">
						<div class="footer-img__wrapper sole-menutop__brand">
							<img class="img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/sole-logo-edited-bot.png' ?>" alt="" class="footer-img">
						</div>
					</div>
					<div class="col-4">
						<div class="footer-contact-us__wrapper">
							<div class="footer-contact-us__headline-wrapper sole-banner__upper-text_position">
								<span class="footer-contact-us__headline">CONTACT US</span>
								<div class="moto__divider-wrapper_outter w-25">
									<div class="moto__divider-wrapper_inner">
										<div class="moto__divider moto__divider_for-small-block d-inline-block"></div>
									</div>
								</div>
							</div>
							<div class="footer-contact-us__mail-wrapper sole-banner__upper-text_position">
								<span class="h6 nunito-semi-bold">solelanguages@gmail.com</span>
							</div>
							<div class="footer-contact-us__numbers-wrapper">
								<span class="h6 nunito-semi-bold">
									00393510424803
								</span>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="footer-follow-us__wrapper sole-banner__upper-text_position">
							<span class="footer-contact-us__headline">FOLLOW US</span>
							<div class="moto__divider-wrapper_outter w-25">
								<div class="moto__divider-wrapper_inner">
									<div class="moto__divider moto__divider_for-small-block d-inline-block"></div>
								</div>
							</div>
						</div>
						<div class="footer__logos-wrapper">
							<a class="d-inline-block sole-menutop__social sole-menutop__social_position" href="#">
								<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/fb.svg' ); ?>
							</a>
							<a class="d-inline-block sole-menutop__social sole-menutop__social_position" href="">
								<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/twit.svg' ); ?>
							</a>
							<a class="d-inline-block sole-menutop__social sole-menutop__social_position" href="#">
								<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/inst.svg' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</body>
</html>