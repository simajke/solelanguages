'use strict';
 
var gulp     = require('gulp'),
sass         = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer'),
livereload   = require('gulp-livereload');
 
sass.compiler= require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./assets/css'))
    // .pipe(livereload());
});
 
gulp.task('sass:watch', function () {
	// livereload.listen();
	gulp.watch('./assets/sass/**/*.scss', gulp.series('sass'));
});