<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<html>
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<?php $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH . "/assets/css/fontawesome/css/all.css"); ?>
		<?php $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH . "/assets/css/fontawesome/css/v4-shims.min.css"); ?>
		<?php $APPLICATION->SetAdditionalCSS( SITE_TEMPLATE_PATH . "/assets/css/style.css"); ?>
	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
<header class="bg-light">
	<section class="sole-menutop">
		<div class="container">
			<nav class="px-0 navbar navbar-expand-lg navbar-light">
			  <a class="navbar-brand sole-menutop__brand sole-menutop__brand_position" href="#">
			  	<img class="img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/sole-logo-edited.png' ?>" alt="">
			  </a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			  <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
			    <ul class="navbar-nav">
			    	<li class="nav-item">
						<a class="nav-link" href="/">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/courses/">Courses</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/about-us/">About Us</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/contact/">Contact</a>
					</li>
					<li class="nav-item sole-menutop__social-wrapper d-flex align-items-center">
						<a class="sole-menutop__social sole-menutop__social_position" href="#">
							<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/fb.svg' ); ?>
						</a>
						<a class="sole-menutop__social sole-menutop__social_position" href="">
							<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/twit.svg' ); ?>
						</a>
						<a class="sole-menutop__social sole-menutop__social_position" href="#">
							<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/inst.svg' ); ?>
						</a>
					</li>
			    </ul>
			  </div>
			</nav>
		</div>
	</section>
</header>	
						