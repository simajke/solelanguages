<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('School Of Linguistic Experience');
?>
<!-- Banner section -->
<section class="category-courses__banner contact-us__banner sole-banner position-relative">
	<div class="transparent-layer absolute-center h-100 w-100"></div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="category-courses__banner-text-wrapper sole-banner__content-wrapper sole-banner__content-wrapper_position">
					<div class="sole-banner__upper-text sole-banner__upper-text_position">
						<h4>CONTACT INFORMATION</h4>
					</div>
					<div class="sole-banner__main-text_wrapper sole-banner__main-text_wrapper-positon">
						<h1 class="h2category-courses__main-text sole-banner__main-text nunito-black">We'd love to<br>hear from you</h1>
						<div class="moto__divider-wrapper_outter">
							<div class="moto__divider-wrapper_inner">
								<div class="moto__divider d-inline-block">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Form section -->
<section class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<div class="why-sole__text-wrapper why-sole__text-wrapper_position sole-courses__text-wrapper sole-courses__text-wrapper-position sole-banner__content-wrapper mb-5">
					<div class="why-sole__headline-wrapper">
						<h2 class="why-sole__headline nunito-black">CONTACT US</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
								
							</div>
						</div>
					</div>
				</div>
				<div class="contact__form-wrapper">
					<form class="contact__form" action="/">
						<div class="contact__name-wrapper contact__label-wrapper d-flex">
							<label class="contact__label contact__label-name" for="name">
								<input class="contact__input" type="text" name="name" id="name">
							</label>
						</div>
						<div class="contact__email-wrapper contact__label-wrapper d-flex">
							<label class="contact__label contact__label-email" for="email">
								<input class="contact__input" type="text" name="email" id="email">
							</label>
						</div>
						<div class="contact__comment-wrapper contact__label-wrapper d-flex">
							<label class="contact__label contact__label-comment" for="comment">
								<textarea name="comment" id="comment" cols="30" rows="10">
									
								</textarea>
							</label>
						</div>
						<div class="sole-banner__button-wrapper">
							<a href="" class="sole-banner__button sole-banner__button_courses">
								<span>Submit Form</span>
								<!-- <span class="ml-1">
						    		<i class="fa fa-long-arrow-right"></i>
						    	</span> -->
							</a>
						</div>
					</form>
				</div>
			</div>
			<div class="col-6">
				<div class="why-sole__text-wrapper why-sole__text-wrapper_position sole-courses__text-wrapper sole-courses__text-wrapper-position sole-banner__content-wrapper mb-5">
					<div class="why-sole__headline-wrapper">
						<h2 class="why-sole__headline nunito-black">CONTACT INFO</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
								
							</div>
						</div>
					</div>
				</div>
				<div class="contact__contact-options">
					<div class="row">
						<div class="col-4">
							<span class="h4 fw-500 ">Email</span>
						</div>
						<div class="col-8">
							<span class="h5 fw-600 ">solelanguages@gmail.com</span>
						</div>
						<div class="w-100 my-2"></div>
						<div class="col-4">
							<span class="h4 fw-500 ">Telephone</span>
						</div>
						<div class="col-8">
							<span class="h5 fw-600 ">00393510424803</span>
						</div>
						<div class="w-100 mt-2 mb-4"></div>
						<div class="col-12 footer__logos-wrapper footer__logos-wrapper_contact">
							<a class="d-inline-block sole-menutop__social sole-menutop__social_contact sole-menutop__social_position" href="#">
								<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/fb.svg' ); ?>
							</a>
							<a class="d-inline-block sole-menutop__social sole-menutop__social_contact sole-menutop__social_position" href="">
								<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/twit.svg' ); ?>
							</a>
							<a class="d-inline-block sole-menutop__social sole-menutop__social_contact sole-menutop__social_position" href="#">
								<?php echo file_get_contents($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/assets/img/inst.svg' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>