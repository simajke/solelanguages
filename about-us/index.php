<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('School Of Linguistic Experience');
?>
<!-- Banner section -->
<section class="category-courses__banner sole-banner position-relative">
	<div class="transparent-layer absolute-center h-100 w-100"></div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="category-courses__banner-text-wrapper sole-banner__content-wrapper sole-banner__content-wrapper_position">
					<div class="sole-banner__upper-text sole-banner__upper-text_position">
						<h4>MEET WITH SOLE</h4>
					</div>
					<div class="sole-banner__main-text_wrapper sole-banner__main-text_wrapper-positon">
						<h1 class="category-courses__main-text sole-banner__main-text nunito-black">About us</h1>
						<div class="moto__divider-wrapper_outter">
							<div class="moto__divider-wrapper_inner">
								<div class="moto__divider d-inline-block">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Sole Sun section -->
<section>
	<div class="container">
		<div class="row align-items-center py-4">
			<div class="col-6">
				<div class="about-us__sole-sun_picture-wrapper">
					<img class="img-fluid about-us__sole-sun_picture" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/sole-sun.png' ?>" alt="">
				</div>
			</div>
			<div class="col-6">
					<div class="moto__content-wrapper">
						<span class="moto__content h2">SOLE, which means ‘sun’ in Italian, is an acronym for the School of Linguistic Experience.</span>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</section>
<!-- What we believe -->
<section>
	<div class="container-fluid about-us__we-believe_background">
		<div class="row">
			<div class="container">
				<div class="row py-5">
					<div class="col-12">
						<div class="moto__content-wrapper">
						<span class="moto__content h3">We believe that language learning and acquisition must go beyond the classroom to truly take hold.</span>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- When created -->
<section>
	<div class="container">
		<div class="row align-items-center py-4">
			<div class="col-6">
					<div class="moto__content-wrapper">
						<span class="moto__content h3">SOLE was born in 2018 with the desire to transform the language learning experience into a community affair.</span>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
							</div>
						</div>
					</div>
			</div>
			<div class="col-6">
				<div class="about-us__sole-sun_picture-wrapper">
					<img class="img-fluid about-us__sole-sun_picture" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/sole-icon.png' ?>" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Teachers section -->
<section class="sole-teachers__container">
	<div class="container">
		<div class="row pb-4">
			<div class="col-12">
				<div class="why-sole__text-wrapper why-sole__text-wrapper_position sole-courses__text-wrapper sole-courses__text-wrapper-position sole-banner__content-wrapper">
					<div class="why-sole__top-text_wrapper whats__second-headline_wrapper-position">
						<span class="why-sole__top-text nunito-regular">MEET OUR TEACHERS</span>
					</div>
					<div class="why-sole__headline-wrapper">
						<h2 class="why-sole__headline nunito-black">TEACHERS</h2>
					</div>
					<div class="moto__divider-wrapper_outter">
						<div class="moto__divider-wrapper_inner">
							<div class="moto__divider d-inline-block">
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="w-100"></div>
			<div class="col-3">
				<div class="sole-teachers__teacher-img-wrapper sole-teachers__teacher-img-wrapper_mid">
					<img class="img-fluid" src="<?php echo SITE_TEMPLATE_PATH . '/assets/img/mal.jpg' ?>" alt="" class="sole-teachers__teacher-img">
				</div>
			</div>
			<div class="col-9 d-flex align-items-center">
				<div class="sole-teachers__inner-wrapper">
					<div class="sole-teachers__headline-wrapper sole-banner__upper-text_position">
						<h4 class="nunito-black card-title m-0">Mallory Echols</h4>
					</div>
					<div class="sole-teachers__text-wrapper sole-banner__upper-text_position">
						<span class="h5">English teacher, the founder and director of SOLE.
						She’s originally from the U.S. and has been living in Sicily since 2017. After earning a Bachelor’s Degree in Spanish with a Business Emphasis from the University of North Georgia in 2010, she worked as an international flight attendant for a U.S. airline. She was initially hired as a Spanish-speaker in the Language of Destination program, and during her career became fluent in and passed her airline’s language tests for both Brazilian Portuguese and Italian classroom principles and community interaction.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>